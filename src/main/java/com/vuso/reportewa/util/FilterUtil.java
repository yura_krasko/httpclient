package com.vuso.reportewa.util;

import com.vuso.reportewa.model.Filter;
import com.vuso.reportewa.model.Params;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

public class FilterUtil {

    /**
     * Create filter
     * @param productId for getting data.
     * @return filter object.
     */
    public static Filter createFilter(final Integer productId) {
        final Filter filterForContracts = new Filter();
        filterForContracts.setFilters(getParamsOfFilter(productId));
        filterForContracts.setLimit(1000);
        filterForContracts.setOffset(0);
        return filterForContracts;
    }

    /**
     * Params of filter.
     * @param productId for getting data.
     * @return list of params.
     */
    private static List<Params> getParamsOfFilter(final Integer productId){
        final List<Params> paramsList = new ArrayList<>();
        paramsList.add(new Params("date", "after", "date", DateUtil.getSelectDate()));
        paramsList.add(new Params("date", "before", "date", DateUtil.getSelectDate()));
        paramsList.add(new Params("state", "in", "list", "SIGNED"));
        paramsList.add(new Params("tariff.id", "eq", "numeric", String.valueOf(productId)));
        return paramsList;
    }
}
