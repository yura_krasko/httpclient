package com.vuso.reportewa.util;

import com.vuso.reportewa.model.Contract;
import org.apache.poi.hssf.usermodel.HSSFHeader;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import java.awt.Color;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileXlsxUtil {

    /**
     * Setting data for header.
     * @param workbook input parameter.
     * @param row input parameter.
     */
    public static void getFileHeader(final XSSFWorkbook workbook, final Row row){
        createCellHeaderStyle(workbook, row, (short)0, "ОСЦВ" );
        createCellHeaderStyle(workbook, row, (short)1, "Дата укладання" );
        createCellHeaderStyle(workbook, row, (short)2, "Дата вступу" );
        createCellHeaderStyle(workbook, row, (short)3, "Дата закінчення дії" );
        createCellHeaderStyle(workbook, row, (short)4, "ПІБ / Найменування страхувальника" );
        createCellHeaderStyle(workbook, row, (short)5, "Марка та модель ТЗ" );
        createCellHeaderStyle(workbook, row, (short)6, "Рік випуску" );
        createCellHeaderStyle(workbook, row, (short)7, "Держ. номер" );
        createCellHeaderStyle(workbook, row, (short)8, "VIN" );
        createCellHeaderStyle(workbook, row, (short)9, "Платеж" );
        createCellHeaderStyle(workbook, row, (short)10, "Категория" );
        createCellHeaderStyle(workbook, row, (short)11, "ID продукта" );
        createCellHeaderStyle(workbook, row, (short)12, "Название продукта" );
        createCellHeaderStyle(workbook, row, (short)13, "Код агента" );
        createCellHeaderStyle(workbook, row, (short)14, "Название заправки" );
    }

    /**
     * Setting date from body
     * @param workbook input parameter.
     * @param row input parameter.
     * @param contract input parameter.
     */
    public static void getFileBody(final XSSFWorkbook workbook, final Row row, final Contract contract){
        createCellBodyStyle(workbook, row, (short) 0, contract.getNumber());
        createCellBodyStyle(workbook, row, (short) 1, getDateFormat(contract.getDate()));
        createCellBodyStyle(workbook, row, (short) 2, getDateFormat(contract.getDateFrom()));
        createCellBodyStyle(workbook, row, (short) 3, getDateFormat(contract.getDateTo()));
        createCellBodyStyle(workbook, row, (short) 4, contract.getCustomer().getName());
        createCellBodyStyle(workbook, row, (short) 5, contract.getInsuranceObject().getModelText());
        createCellBodyStyle(workbook, row, (short) 6, contract.getInsuranceObject().getYear().toString());
        createCellBodyStyle(workbook, row, (short) 7, contract.getInsuranceObject().getStateNumber());
        createCellBodyStyle(workbook, row, (short) 8, contract.getInsuranceObject().getBodyNumber());
        createCellBodyStyle(workbook, row, (short) 9, contract.getBrokerDiscountedPayment().toString());
        createCellBodyStyle(workbook, row, (short) 10, contract.getInsuranceObject().getCategory());
        createCellBodyStyle(workbook, row, (short) 11, contract.getTariff().getId().toString());
        createCellBodyStyle(workbook, row, (short) 12, contract.getTariff().getName());
        createCellBodyStyle(workbook, row, (short) 13, contract.getSalePoint().getReportSalePoint().getCode());
        createCellBodyStyle(workbook, row, (short) 14, contract.getSalePoint().getReportSalePoint().getName());
    }

    /**
     * Style for header.
     * @param workbook input parameter.
     * @param row input parameter.
     * @param column input parameter.
     * @param nameColumn input parameter.
     */
    private static void createCellHeaderStyle(final Workbook workbook, final Row row, final Short column, final String nameColumn) {
        XSSFFont font= (XSSFFont) workbook.createFont();
        font.setFontHeightInPoints((short)12);
        font.setFontName("Arial");
        font.setBold(true);
        font.setItalic(true);
        XSSFCellStyle styleHeader = (XSSFCellStyle) workbook.createCellStyle();
        styleHeader.setAlignment(HorizontalAlignment.CENTER);
        styleHeader.setFont(font);
        XSSFCell cell = (XSSFCell) row.createCell(column);
        cell.setCellValue(nameColumn);
        cell.setCellStyle(styleHeader);
    }


    /**
     * Style for body.
     * @param workbook input parameter.
     * @param row input parameter.
     * @param column input parameter.
     * @param nameColumn  input parameter.
     */
    private static void createCellBodyStyle(final Workbook workbook, final Row row, final Short column, final String nameColumn) {
        XSSFFont font= (XSSFFont) workbook.createFont();
        font.setFontHeightInPoints((short)10);
        font.setFontName("Arial");
        Cell cell = row.createCell(column);
        cell.setCellValue(nameColumn);
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(font);
        cell.setCellStyle(cellStyle);
    }

    /**
     * Converting date from format "yyyy-MM-dd" to "dd.MM.yyyy".
     * @param date in format "yyyy-MM-dd".
     * @return date in format "dd.MM.yyyy".
     */
    private static String getDateFormat(final String date){
        SimpleDateFormat dateFormatOld = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateFormatNew = new SimpleDateFormat("dd.MM.yyyy");
        Date dateParse = null;
        try {
            dateParse = dateFormatOld.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormatNew.format(dateParse);
    }
}
