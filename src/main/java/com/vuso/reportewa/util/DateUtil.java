package com.vuso.reportewa.util;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtil {

    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Date of selection data from EWA(current date -1 day)
     * @return date to format yyyy-MM-dd.
     */
    public static String getSelectDate(){
        final String selectDate;
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        selectDate = dateFormat.format(calendar.getTime());
        return selectDate;
    }
}
