package com.vuso.reportewa.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.nio.file.Path;

@Configuration
public class AppConfig {

    @Bean
    public HttpClient httpClient(){
        return HttpClientBuilder.create().build();
    }

    @Bean
    public Gson getGson(){
        return new GsonBuilder().create();
    }

    @Bean
    public XSSFWorkbook workbook(){
        return new XSSFWorkbook();
    }

}
