package com.vuso.reportewa.service;

public interface EwaService {

    /**
     * Getting cookie EWA.
     * @param url for autorization.
     * @return cookie.
     */
    String getCookie(final String url);

    /**
     * Getting contracts with EWA.
     * @param url for getting contracts.
     * @param cookie for getting data.
     * @param productsId for getting data.
     * @return list of contracts in format json.
     */
    String getContracts(final String url, final String cookie, final Integer productsId);

    /**
     * Logout EWA.
     */
    void logoutEwa(final String url);
}
