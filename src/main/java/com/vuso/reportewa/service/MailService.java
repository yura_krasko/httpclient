package com.vuso.reportewa.service;

import com.vuso.reportewa.model.Mail;

public interface MailService {
    /**
     * Sending simple text message.
     * @param mailTo mail address to send message.
     * @param subject message.
     * @param message text.
     */
    void sendMessage(final String mailTo, final String subject, final String message);

    /**
     * Sending message with attachment.
     * @param mail message.
     */
    void sendAttachment(final Mail mail);
}
