package com.vuso.reportewa.service;

import com.google.gson.Gson;
import com.vuso.reportewa.util.FilterUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class EwaServiseImpl implements EwaService {

    private static final Logger logger = LoggerFactory.getLogger(EwaServiseImpl.class);

    @Autowired
    private  HttpClient httpClient;

    @Autowired
    Gson gson;

    @Value("${ewa.authorization.user}")
    private String username;

    @Value("${ewa.authorization.password}")
    private String password;

    /**
     * Implementing getting cookie  EWA.
     * @param url for autorization.
     * @return cookie.
     */
    @Override
    public String getCookie(final String url) {
        HttpResponse response = null;
        try {
            final HttpPost postRequest = new HttpPost(url);
            postRequest.setHeader("Content-Type", "application/x-www-form-urlencoded");
            final List<NameValuePair> requestParams = new ArrayList<>();
            requestParams.add(new BasicNameValuePair("email", username));
            requestParams.add(new BasicNameValuePair("password", password));
            postRequest.setEntity(new UrlEncodedFormEntity(requestParams));
            response = httpClient.execute(postRequest);
            logger.info("Authorization is successful!");
        }  catch (IOException e) {
            logger.debug("Error with authorization in EWA. Exception:" + e);
        }
        return response.getFirstHeader("Set-Cookie").toString();
    }

    /**
     * Implementing getting contracts with EWA.
     * @param url for getting contracts.
     * @param cookie for getting data.
     * @param productsId for getting data.
     * @return list of contracts in format json.
     */
    @Override
    public String getContracts(final String url, final String cookie, final Integer productsId) {
        String responseString = null;
        try {
            final HttpPost postRequest = new HttpPost(url);
            postRequest.setHeader("Content-Type", "application/json; charset=utf-8");
            postRequest.setHeader("Cookie", cookie);
            String filter = gson.toJson(FilterUtil.createFilter(productsId));
            StringEntity stringEntity = new StringEntity(filter);
            postRequest.setEntity(stringEntity);
            HttpResponse response = httpClient.execute(postRequest);
            HttpEntity entity = response.getEntity();
            responseString = EntityUtils.toString(entity, "UTF-8");
            logger.info("Getting data is successful!");
        } catch (IOException e) {
            logger.debug("Error getting data with EWA. Exception:" + e);
        }
        return responseString;
    }

    /**
     * Implementing logout EWA.
     */
    @Override
    public void logoutEwa(final String url) {
        try {
            final HttpPut putRequest = new HttpPut(url);
            putRequest.setHeader("Content-Type", "application/json; charset=utf-8");
            httpClient.execute(putRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
