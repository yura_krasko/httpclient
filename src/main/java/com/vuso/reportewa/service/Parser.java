package com.vuso.reportewa.service;

import java.util.List;

public interface Parser<T> {

    /**
     * Getting contract WOG object from json and parse.
     * @param jsonData string of json data.
     * @return list of contract objects.
     */
    List<T> parseContractWog(final String jsonData);

    /**
     * Getting contract BRSM object from json and parse.
     * @param jsonData string of json data.
     * @return list of contract objects.
     */
    List<T> parseContractSocar(final String jsonData);
}
