package com.vuso.reportewa.service;

import com.vuso.reportewa.model.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.internet.MimeMessage;
import java.io.File;

@Service
public class MailServiceImpl implements MailService {

    private static final Logger logger = LoggerFactory.getLogger(MailServiceImpl.class);

    @Value("${spring.mail.username}")
    private String userName;


    @Autowired
    private JavaMailSender mailSender;

    /**
     * Implementing sending simple text message.
     * @param mailTo mail address to send message.
     * @param subject message.
     * @param message text.
     */
    @Override
    public void sendMessage(final String mailTo, final String subject, final String message){
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(userName);
        mailMessage.setTo(mailTo);
        mailMessage.setSubject(subject);
        mailMessage.setText(message);
        mailSender.send(mailMessage);
    }

    /**
     * Implementing sending message with attachment.
     * @param mail message.
     */
    @Override
    public void sendAttachment(final Mail mail) {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper;
        try {
            helper = new MimeMessageHelper(message, true, "UTF-8");
            helper.setFrom(userName);
            helper.setTo(mail.getTo());
            helper.setSubject(mail.getSubject());
            helper.setText(mail.getContent());
            helper.addAttachment("report.xlsx", new ClassPathResource(mail.getPathToFile()));
            mailSender.send(message);
        } catch (Exception e) {
            logger.debug("Message is not send, exception: " + e.getMessage());
        }
    }
}
