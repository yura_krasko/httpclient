package com.vuso.reportewa.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vuso.reportewa.model.Contract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ParserImpl implements Parser {

    @Autowired
    Gson gson;

    /**
     * Implementing getting contract WOG object from json and parse.
     * @param jsonData string of json data.
     * @return list of contract objects.
     */
    @Override
    public List<Contract> parseContractWog(String jsonData) {
        TypeToken<List<Contract>> token = new TypeToken<List<Contract>>() {};
        List<Contract> contracts = gson.fromJson(jsonData, token.getType());
        List<Contract> contractsWog = new ArrayList<>();
        for (Contract contract : contracts){
            if (contract.getBrokerDiscountedPayment() >= 405
                    & (!contract.getInsuranceObject().getCategory().equals("F") && contract.getSalePoint().getReportSalePoint().getName().equals("WOG"))
                    & ( !contract.getInsuranceObject().getCategory().equals("E") && contract.getSalePoint().getReportSalePoint().getName().equals("WOG"))
                    & ( !contract.getInsuranceObject().getCategory().equals("A1") && contract.getSalePoint().getReportSalePoint().getName().equals("WOG"))
                    & ( !contract.getInsuranceObject().getCategory().equals("A2") && contract.getSalePoint().getReportSalePoint().getName().equals("WOG"))){
                contractsWog.add(contract);
            }
        }
        return contractsWog;
    }

    /**
     * Implementing getting contract BRSM object from json and parse.
     * @param jsonData string of json data.
     * @return list of contract objects.
     */
    @Override
    public List<Contract> parseContractSocar(String jsonData) {
        TypeToken<List<Contract>> token = new TypeToken<List<Contract>>() {};
        List<Contract> contracts = gson.fromJson(jsonData, token.getType());
        List<Contract> contractsSocar= new ArrayList<>();
        for (Contract contract : contracts){
            if ((!contract.getInsuranceObject().getCategory().equals("F") && !contract.getSalePoint().getReportSalePoint().getName().equals("KLO"))
                    & ( !contract.getInsuranceObject().getCategory().equals("E") && !contract.getSalePoint().getReportSalePoint().getName().equals("KLO"))
                    & ( !contract.getInsuranceObject().getCategory().equals("A1") && !contract.getSalePoint().getReportSalePoint().getName().equals("KLO"))
                    & ( !contract.getInsuranceObject().getCategory().equals("A2") && !contract.getSalePoint().getReportSalePoint().getName().equals("KLO"))) {
                contractsSocar.add(contract);
            }
        }
        return contractsSocar;
    }
}
