package com.vuso.reportewa.service;

import com.vuso.reportewa.model.Contract;
import com.vuso.reportewa.util.FileXlsxUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class XlsxServiceImpl implements XlsxService{

    /**
     * Implementing creating xslx file.
     * @param contracts list of contracts.
     * @return workbook.
     */
    @Override
    public XSSFWorkbook createWorkbook(final List<Contract> contracts) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("ОСАГО");
        sheet.getDefaultColumnWidth();
        Row rowHeader = sheet.createRow(0);
        rowHeader.setHeightInPoints(30);
        sheet.setColumnWidth(0, 5000);
        FileXlsxUtil.getFileHeader(workbook, rowHeader);
        int i = 1;
        Row rowBody;
        for (Contract contract : contracts){
            rowBody = sheet.createRow(i++);
            FileXlsxUtil.getFileBody(workbook, rowBody, contract);
        }
        return workbook;
    }

    /**
     * Implementing writing xslx file.
     * @param path path for writting file.
     * @param workbook workbook.
     */
    @Override
    public void writeXslxFile(String path, final XSSFWorkbook workbook) {
        try (FileOutputStream out = new FileOutputStream(new File(path))){
            workbook.write(out);
            out.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
