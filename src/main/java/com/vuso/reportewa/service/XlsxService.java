package com.vuso.reportewa.service;

import com.vuso.reportewa.model.Contract;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

public interface XlsxService {

    /**
     * Create xslx file.
     * @param contracts list of contracts.
     * @return workbook.
     */
    XSSFWorkbook createWorkbook(final List<Contract> contracts);

    /**
     * Write xslx file.
     * @param path path for writting file.
     * @param workbook workbook.
     */
    void writeXslxFile(final String path, final XSSFWorkbook workbook);
}
