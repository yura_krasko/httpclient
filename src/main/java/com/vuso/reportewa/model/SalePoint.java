package com.vuso.reportewa.model;

import java.util.Map;

public class SalePoint {

    private Long id;
    private Map<String, Object> company;
    private String code;
    private String name;
    private String namePrint;
    private Long lft;
    private Long rgt;
    private String path;
    private Boolean paymentsAvailable;
    private ReportSalePoint reportSalePoint;

    public SalePoint() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Map<String, Object> getCompany() {
        return company;
    }

    public void setCompany(Map<String, Object> company) {
        this.company = company;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamePrint() {
        return namePrint;
    }

    public void setNamePrint(String namePrint) {
        this.namePrint = namePrint;
    }

    public Long getLft() {
        return lft;
    }

    public void setLft(Long lft) {
        this.lft = lft;
    }

    public Long getRgt() {
        return rgt;
    }

    public void setRgt(Long rgt) {
        this.rgt = rgt;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getPaymentsAvailable() {
        return paymentsAvailable;
    }

    public void setPaymentsAvailable(Boolean paymentsAvailable) {
        this.paymentsAvailable = paymentsAvailable;
    }

    public ReportSalePoint getReportSalePoint() {
        return reportSalePoint;
    }

    public void setReportSalePoint(ReportSalePoint reportSalePoint) {
        this.reportSalePoint = reportSalePoint;
    }
}
