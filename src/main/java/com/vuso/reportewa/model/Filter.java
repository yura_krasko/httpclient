package com.vuso.reportewa.model;

import java.util.List;

public class Filter {

    private List<Params> filters;
    private Integer limit;
    private Integer offset;

    public Filter() {
    }

    public List<Params> getFilters() {
        return filters;
    }

    public void setFilters(List<Params> filters) {
        this.filters = filters;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }
}
