package com.vuso.reportewa.model;

import java.math.BigDecimal;
import java.util.Map;

public class Auto {

    private String type;
    private Integer id;
    private BigDecimal payment;
    private String risks;
    private String customFields;
    private String category;
    private Map<String, Object> model;
    private String lastModified;
    private String modelText;
    private String bodyNumber;
    private String dontHaveBodyNumber;
    private String stateNumber;
    private String dontHaveStateNumber;
    private Map<String, Object> registrationPlace;
    private String outsideUkraine;
    private String registrationType;
    private Integer year;
    private String engineVolume;
    private String complectation;

    public Auto() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public String getRisks() {
        return risks;
    }

    public void setRisks(String risks) {
        this.risks = risks;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Map<String, Object> getModel() {
        return model;
    }

    public void setModel(Map<String, Object> model) {
        this.model = model;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getModelText() {
        return modelText;
    }

    public void setModelText(String modelText) {
        this.modelText = modelText;
    }

    public String getBodyNumber() {
        return bodyNumber;
    }

    public void setBodyNumber(String bodyNumber) {
        this.bodyNumber = bodyNumber;
    }

    public String getDontHaveBodyNumber() {
        return dontHaveBodyNumber;
    }

    public void setDontHaveBodyNumber(String dontHaveBodyNumber) {
        this.dontHaveBodyNumber = dontHaveBodyNumber;
    }

    public String getStateNumber() {
        return stateNumber;
    }

    public void setStateNumber(String stateNumber) {
        this.stateNumber = stateNumber;
    }

    public String getDontHaveStateNumber() {
        return dontHaveStateNumber;
    }

    public void setDontHaveStateNumber(String dontHaveStateNumber) {
        this.dontHaveStateNumber = dontHaveStateNumber;
    }

    public Map<String, Object> getRegistrationPlace() {
        return registrationPlace;
    }

    public void setRegistrationPlace(Map<String, Object> registrationPlace) {
        this.registrationPlace = registrationPlace;
    }

    public String getOutsideUkraine() {
        return outsideUkraine;
    }

    public void setOutsideUkraine(String outsideUkraine) {
        this.outsideUkraine = outsideUkraine;
    }

    public String getRegistrationType() {
        return registrationType;
    }

    public void setRegistrationType(String registrationType) {
        this.registrationType = registrationType;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getEngineVolume() {
        return engineVolume;
    }

    public void setEngineVolume(String engineVolume) {
        this.engineVolume = engineVolume;
    }

    public String getComplectation() {
        return complectation;
    }

    public void setComplectation(String complectation) {
        this.complectation = complectation;
    }
}
