package com.vuso.reportewa.model;

import java.util.Map;

public class Tarif {

    private String type;
    private Integer id;
    private String name;
    private Map<String, Object> company;
    private Map<String, Object> insurer;
    private Map<String, Object> contractType;
    private String dateFrom;
    private String state;
    private String objectsCalcType;
    private String contractDateLimitType;
    private String contractDateLimit;
    private Boolean risksSupported;
    private Boolean risksOnContract;
    private Boolean signerOtpRequired;
    private Boolean customerOtpRequired;

    public Tarif() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getCompany() {
        return company;
    }

    public void setCompany(Map<String, Object> company) {
        this.company = company;
    }

    public Map<String, Object> getInsurer() {
        return insurer;
    }

    public void setInsurer(Map<String, Object> insurer) {
        this.insurer = insurer;
    }

    public Map<String, Object> getContractType() {
        return contractType;
    }

    public void setContractType(Map<String, Object> contractType) {
        this.contractType = contractType;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getObjectsCalcType() {
        return objectsCalcType;
    }

    public void setObjectsCalcType(String objectsCalcType) {
        this.objectsCalcType = objectsCalcType;
    }

    public String getContractDateLimitType() {
        return contractDateLimitType;
    }

    public void setContractDateLimitType(String contractDateLimitType) {
        this.contractDateLimitType = contractDateLimitType;
    }

    public Boolean getRisksSupported() {
        return risksSupported;
    }

    public void setRisksSupported(Boolean risksSupported) {
        this.risksSupported = risksSupported;
    }

    public Boolean getRisksOnContract() {
        return risksOnContract;
    }

    public void setRisksOnContract(Boolean risksOnContract) {
        this.risksOnContract = risksOnContract;
    }

    public Boolean getSignerOtpRequired() {
        return signerOtpRequired;
    }

    public void setSignerOtpRequired(Boolean signerOtpRequired) {
        this.signerOtpRequired = signerOtpRequired;
    }

    public Boolean getCustomerOtpRequired() {
        return customerOtpRequired;
    }

    public void setCustomerOtpRequired(Boolean customerOtpRequired) {
        this.customerOtpRequired = customerOtpRequired;
    }

    public String getContractDateLimit() {
        return contractDateLimit;
    }

    public void setContractDateLimit(String contractDateLimit) {
        this.contractDateLimit = contractDateLimit;
    }
}
