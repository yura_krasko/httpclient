package com.vuso.reportewa.model;

public class Params {

    private String field;
    private String comparison;
    private String type;
    private String value;

    public Params() {
    }

    public Params(String field, String comparison, String type, String value) {
        this.field = field;
        this.comparison = comparison;
        this.type = type;
        this.value = value;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getComparison() {
        return comparison;
    }

    public void setComparison(String comparison) {
        this.comparison = comparison;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
