package com.vuso.reportewa;

import com.vuso.reportewa.model.Contract;
import com.vuso.reportewa.model.Mail;
import com.vuso.reportewa.service.Parser;
import com.vuso.reportewa.service.EwaService;
import com.vuso.reportewa.service.MailService;
import com.vuso.reportewa.service.XlsxService;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class ReportewaApplication implements CommandLineRunner {

    @Autowired
    EwaService ewaService;

    @Autowired
    XlsxService xlsxService;

    @Autowired
    Parser deserializationJson;

    @Autowired
    XSSFWorkbook workbook;

    @Autowired
    private MailService mailService;

    @Value("${ewa.authorization.url}")
    private String authorizationUrl;

    @Value("${ewa.contracts.url}")
    private String contractsUrl;

    @Value("${ewa.logout.url}")
    private String logoutUrl;

    @Value("${mail.recipients.wog}")
    private String [] recipientsWog;

    @Value("${mail.recipients.brsm}")
    private String [] recipientsBrsm;

    private Integer[] productsWog = {9702, 9705, 9598, 9600, 9707, 9706, 10225};
    private Integer[] productsSocar = {10780, 10743, 10742, 10740, 10741};


    @Value("${mail.attachment.path.wog}")
    private String pathWogAttachment;

    @Value("${mail.attachment.path.brsm}")
    private String pathSocarAttachment;

    @Value("${file.path.wog}")
    private String pathWogFile;

    @Value("${file.path.socar}")
    private String pathSocaFile;


    public static void main(String[] args) {
        SpringApplication.run(ReportewaApplication.class, args);
    }


    @Override
    public void run(String... args) {


        String cookie = ewaService.getCookie(authorizationUrl);
        List<Contract> contractsWog = new ArrayList<>();
        for (int i = 0; i < productsWog.length; i++) {
            String json = ewaService.getContracts(contractsUrl, cookie, productsWog[i]);
            System.out.println(json);
            contractsWog.addAll(deserializationJson.parseContractWog(json));
        }
        workbook =  xlsxService.createWorkbook(contractsWog);
        xlsxService.writeXslxFile(pathWogFile, workbook);

        Mail mailWog = new Mail();
        mailWog.setTo(recipientsWog);
        mailWog.setSubject("Отчет о продажах Юниверсал Ассистанс");
        mailWog.setContent("Отчет о продажах Юниверсал Ассистанс");
        mailWog.setPathToFile(pathWogAttachment);
        mailService.sendAttachment(mailWog);

        List<Contract> contractsBrsm = new ArrayList<>();
        for (int i = 0; i < productsSocar.length; i++) {
            String json = ewaService.getContracts(contractsUrl, cookie, productsSocar[i]);
            System.out.println(json);
            contractsBrsm.addAll(deserializationJson.parseContractSocar(json));
        }
        workbook =  xlsxService.createWorkbook(contractsBrsm);
        xlsxService.writeXslxFile(pathSocaFile, workbook);
        ewaService.logoutEwa(logoutUrl);

        Mail mailbrsm = new Mail();
        mailbrsm.setTo(recipientsBrsm);
        mailbrsm.setSubject("Отчет о продажах Юниверсал Ассистанс");
        mailbrsm.setContent("Отчет о продажах Юниверсал Ассистанс");
        mailbrsm.setPathToFile(pathSocarAttachment);
        mailService.sendAttachment(mailbrsm);
    }
}
